<?php
include_once 'src/Helper/index.php';
include_once 'src/Resource/includeResource.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Zooin - Plataforma de vendas online</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="/assets/css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/assets/css/jumbotron.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Zooin - Loja Online</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <?php
            session_start();
            if (isset($_SESSION['user'])):
                $carrinho = new Carrinho();
                ?>
                <div class="navbar-form navbar-right carrinho">
                    <div class="form-group">
                        <label>Olá <?php echo $_SESSION['user']->nome ?>, você possui
                            <?php echo $carrinho->count($_SESSION['user']->id); ?> item(s) no carrinho.</label>
                    </div>
                    <div class="form-group">
                        <?php if ($_SESSION['user']->admin == 1): ?>
                            <a href="/admin.php" class="btn btn-success">Painel</a>
                        <?php endif; ?>
                        <a href="index.php?pag=carrinho" class="btn btn-success">Ver meu carrinho</a>
                        <a href="index.php?pag=logout" class="btn btn-success">Sair</a>
                    </div>
                </div>
                <?php
            else:
                ?>
                <form action="index.php?pag=login" method="POST" class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="text" name="email" placeholder="Email" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" placeholder="Senha" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">Acessar minha conta</button>
                </form>
                <?php
            endif;
            ?>
        </div><!--/.navbar-collapse -->
    </div>
</nav>
<section>
    <?php
    /*
     * Padroniza as urls como /?pag=pagina-de-destino para não precisar ficar recarregando tudo o tempo todo,
     * facilitar a codificação
     */
    if (isset($_GET['pag'])) {
        switch ($_GET['pag']) {
            case 'principal':
                include_once 'pages/client/principal/principal.php';
                break;
            case 'registro':
                include_once 'pages/client/clientes/registro.php';
                break;
            case 'logout':
                include_once 'pages/client/clientes/logout.php';
                break;
            case 'login':
                include_once 'pages/client/clientes/login.php';
                break;
            case 'carrinho':
                include_once 'pages/client/clientes/carrinho.php';
                break;
            case 'produto':
                include_once 'pages/client/conteudo/adicionarCarrinho.php';
                break;
            default:
                include_once 'pages/client/principal/principal.php';
                break;
        }
    } else {
        include_once 'pages/client/principal/principal.php';
    }
    ?>
</section>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/docs.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>