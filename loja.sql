-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 17-Jun-2017 às 02:05
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loja`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `armacao`
--

CREATE TABLE `armacao` (
  `id` int(5) NOT NULL,
  `marca` varchar(20) NOT NULL,
  `modelo` varchar(20) NOT NULL,
  `qtd` int(5) NOT NULL,
  `valorunit` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `armacao`
--

INSERT INTO `armacao` (`id`, `marca`, `modelo`, `qtd`, `valorunit`) VALUES
(2, 'LeoLeo', 'LeoLeo1', 100, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `id` int(5) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `cpf` char(14) NOT NULL,
  `email` varchar(20) NOT NULL,
  `sexo` varchar(9) NOT NULL,
  `cel` varchar(13) NOT NULL,
  `telfixo` varchar(12) NOT NULL,
  `cep` char(9) NOT NULL,
  `logradouro` varchar(45) NOT NULL,
  `bairro` varchar(45) NOT NULL,
  `cidade` varchar(32) NOT NULL,
  `estado` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `cpf`, `email`, `sexo`, `cel`, `telfixo`, `cep`, `logradouro`, `bairro`, `cidade`, `estado`) VALUES
(2, 'Andre Felipe', '619.298.403-49', 'kiielcb@gmail.com', 'Masculino', '61 99557-1332', '61 3212-2357', '71939-360', 'Quadra 203', 'Sul (Águas Claras)', 'Brasília', 'DF');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedor`
--

CREATE TABLE `fornecedor` (
  `id` int(5) NOT NULL,
  `razaosocial` varchar(45) NOT NULL,
  `cnpj` varchar(18) NOT NULL,
  `email` varchar(20) NOT NULL,
  `telfixo` varchar(12) NOT NULL,
  `cep` varchar(9) NOT NULL,
  `logradouro` varchar(45) NOT NULL,
  `bairro` varchar(45) NOT NULL,
  `cidade` varchar(32) NOT NULL,
  `estado` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE `funcionario` (
  `id` int(5) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `cpf` bigint(11) NOT NULL,
  `email` varchar(20) NOT NULL,
  `sexo` enum('M','F') NOT NULL,
  `cel` int(9) NOT NULL,
  `telfixo` int(9) NOT NULL,
  `cep` int(8) NOT NULL,
  `logradouro` varchar(45) NOT NULL,
  `bairro` varchar(45) NOT NULL,
  `cidade` varchar(32) NOT NULL,
  `estado` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `lente`
--

CREATE TABLE `lente` (
  `id` int(5) NOT NULL,
  `marca` varchar(20) NOT NULL,
  `modelo` varchar(20) NOT NULL,
  `grau` int(3) NOT NULL,
  `qtd` int(5) NOT NULL,
  `valorunit` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `lente`
--

INSERT INTO `lente` (`id`, `marca`, `modelo`, `grau`, `qtd`, `valorunit`) VALUES
(1, 'LeoLeo', 'LeoLeo1', 12, 200, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `armacao`
--
ALTER TABLE `armacao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fornecedor`
--
ALTER TABLE `fornecedor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lente`
--
ALTER TABLE `lente`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `armacao`
--
ALTER TABLE `armacao`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fornecedor`
--
ALTER TABLE `fornecedor`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `funcionario`
--
ALTER TABLE `funcionario`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lente`
--
ALTER TABLE `lente`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
