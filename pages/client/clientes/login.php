<?php

if (isset($_POST) && isset($_POST['email']) && isset($_POST['password'])) {
    $cliente = new Clientes();
    $result = $cliente->login($_POST['email'], $_POST['password']);

    if (count($result) > 0) {
        $_SESSION['user'] = $result;
        echo '<script> window.location = "/"; </script>';
    } else {
        echo '<script> alert("Usuário ou senha incorretos, tente novamente."); window.location = "/"; </script>';
    }
} else {
    echo '<script> alert("Usuário ou senha incorretos, tente novamente."); window.location = "/"; </script>';
}