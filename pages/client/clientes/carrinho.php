<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>Carrinho de compras</h1>
        <p>Não perca tempo, feche negócio hoje.</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Pagar com PagSeguro &raquo;</a></p>
    </div>
</div>

<div class="container">
    <h2>Você escolheu:</h2>
    <div class="row">
        <?php
        if(!isset($_SESSION['user'])){
            echo '<script>location.href = "/";</script>';
        }
        $resultado = new Carrinho();
        $resultado = $resultado->findAll($_SESSION['user']->id);

        foreach ($resultado as $item):
            ?>
            <div class="col-md-12">
                <h3>Produto: <?php echo $item->produto; ?></h3>
                <h4>Preço: <?php echo $item->preco; ?></h4>
                <p>Quantidade: <?php echo $item->quantidade; ?></p>
            </div>
            <?php
        endforeach;
        ?>
    </div>
</div>
<br>
<br>
<br>
<br>
<footer>
    <p>&copy; <?php echo date('Y'); ?> Zooin Company LTDA - Todos os direitos reservados.</p>
</footer>
</div> <!-- /container -->