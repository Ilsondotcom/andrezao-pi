<?php
$var = $carrinho->add([
    'produto' => $_GET['produto'],
    'preco' => $_GET['preco'],
    'quantidade' => 1,
    'cliente' => $_SESSION['user']->id
]);

if ($var) {
    echo '<script>alert("Produto adicionado ao carrinho!");location.href = "/?pag=carrinho";</script>';
} else {
    echo '<script>alert("Erro ao adicionar produto no carrinho!");location.href = "/?pag=principal";</script>';
}