<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>Bem vindos!</h1>
        <p>Conheça a mais nova forma de montar o seu óculos, sem sair de casa.</p>
        <?php if(!isset($_SESSION['user'])): ?>
        <p>Faça o login para comprar produtos ou...</p>
        <p><a class="btn btn-primary btn-lg" href="?pag=registro" role="button">Criar conta &raquo;</a></p>
        <?php endif; ?>
    </div>
</div>

<div class="container">
    <h1>Armações</h1>
    <div class="row">
        <?php
            $armacoes = new Armacao();
            $result = $armacoes->findAll();

            foreach ($result as $item):
        ?>
        <div class="col-md-4">
            <h3><?php echo $item->modelo; ?></h3>
            <h4><?php echo $item->marca; ?></h4>
            <p><?php echo $item->descricao; ?></p>
            <?php if(isset($_SESSION['user'])): ?>
            <p><a class="btn btn-primary"
                  href="/?pag=produto&produto=<?php echo $item->modelo; ?>&preco=<?php echo $item->valorunit; ?>"
                  role="button">Comprar &raquo;</a></p>
            <?php endif; ?>
        </div>
        <?php
            endforeach;
        ?>
    </div>
</div>
<br>
<br>
<div class="container">
    <h1>Lentes</h1>
    <div class="row">
        <?php
        $lentes = new Lente();
        $lentes = $lentes->findAll();

        foreach ($lentes as $item):
            ?>
            <div class="col-md-4">
                <h3><?php echo $item->modelo; ?></h3>
                <h4><?php echo $item->marca; ?></h4>
                <p><?php echo $item->descricao; ?></p>
                <?php if(isset($_SESSION['user'])): ?>
                    <p><a class="btn btn-primary"
                          href="/?pag=produto&produto=<?php echo $item->modelo; ?>&preco=<?php echo $item->valorunit; ?>"
                          role="button">Comprar &raquo;</a></p>
                <?php endif; ?>
            </div>
            <?php
        endforeach;
        ?>
    </div>
</div>

    <footer>
        <p>&copy; <?php echo date('Y'); ?> Zooin Company LTDA - Todos os direitos reservados.</p>
    </footer>
</div> <!-- /container -->