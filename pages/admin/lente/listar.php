<?php
?>
<div class="row">
    <div class="col-xs-12">
        <h1>Lentes</h1>
        <table class="table table-striped" border="1">
            <thead>
            <tr>
                <th>#</th>
                <th>Marca</th>
				<th>Modelo</th>
                <th>Grau</th>
				<th>Quantidade</th>
				<th>Valor Unitário</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $lente = new Lente();
                $result = $lente->findAll();

                foreach ($result as $row):
            ?>
            <tr>
                <td><?php echo $row->id; ?></td>
                <td><?php echo $row->marca; ?></td>
				<td><?php echo $row->modelo; ?></td>
                <td><?php echo $row->grau; ?></td>
				<td><?php echo $row->qtd ; ?></td>
				<td><?php echo $row->valorunit; ?></td>
                <td>
                    <a href="/admin.php?pag=lente&acao=editar&id=<?php echo $row->id; ?>">Editar</a> |
                    <a onclick="return confirm('Deseja realmente excluir esse registro?')" href="/admin.php?pag=lente&acao=excluir&id=<?php echo $row->id; ?>">Excluir</a>
                </td>
            </tr>
            <?php
                endforeach;
            ?>
            </tbody>
        </table>
    </div>
</div>
