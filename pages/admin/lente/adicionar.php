<div class="row">
    <div class="col-xs-12">
        <h1>Lente</h1>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Registro de Lentes</h3>
            </div>
            <div class="panel-body">
                <form action="admin.php?pag=lente&acao=cadastrar" method="POST">
                    <div class="form-group">
                        <label for="marca">Marca</label>
                        <input type="text" class="form-control" id="marca" name="marca" placeholder="Marca">
                    </div>
                    <div class="form-group">
                        <label for="modelo">Modelo</label>
                        <input type="text" class="form-control" id="modelo" name="modelo" placeholder="Modelo">
                    </div>
                    <div class="form-group">
                        <label for="grau">Grau</label>
                        <input type="text" class="form-control" id="grau" name="grau" placeholder="Grau">
                    </div>
                    <div class="form-group">
                        <label for="qtd">Quantidade</label>
                        <input type="text" class="form-control" id="qtd" placeholder="Quantidade" name="qtd">
                    </div>
					<div class="form-group">
                        <label for="valorunit">Valor Unitário</label>
                        <input type="text" class="form-control" id="valorunit" placeholder="Valor Unitário" name="valorunit">
                    </div>                   
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </form>
            </div>
        </div>
    </div>
</div>