<?php

if (isset($_POST)) {

    $lente = new Lente();
    $result = $lente->add($_POST);

    if ($result) {
        echo '<script>alert("Cadastrado com sucesso!");location.href = "/admin.php?pag=lente&acao=listar";</script>';
    } else {
        echo '<script>alert("Erro ao cadastrar!");location.href = "/admin.php?pag=lente&acao=cadastrar";</script>';
    }
} else {
    include_once 'pages/erros/erro403.php';
}
?>