<?php
if (isset($_POST)) {

    $lente = new Lente();
    $result = $lente->update($_POST);

    if ($result) {
        echo '<script>alert("Alterado com sucesso!");location.href = "/admin.php?pag=lente&acao=listar";</script>';
    } else {
        echo '<script>alert("Erro ao alterar!");location.href = "/admin.php?pag=lente&acao=editar&id='. $_POST['id'].'";</script>';
    }
} else {
    include_once 'pages/erros/erro403.php';
}
?>
