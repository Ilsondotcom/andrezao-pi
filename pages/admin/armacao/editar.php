<?php
    $armacao = new Armacao();
    $data = $armacao->findOneBy($_GET['id']);
?>
<div class="row">
    <div class="col-xs-12">
        <h1>Armação</h1>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Registro de Armações</h3>
            </div>
            <div class="panel-body">
                <form action="/admin.php?pag=armacao&acao=alterar&id=<?php echo $data->id ?>" method="POST">
                    <div class="form-group">
                        <label for="marca">Marca</label>
                        <input type="text" class="form-control" value="<?php echo $data->marca ?>" id="marca" name="marca"
                               placeholder="Marca">
                    </div>
                    <div class="form-group">
                        <label for="modelo">Modelo</label>
                        <input type="text" class="form-control" value="<?php echo $data->modelo ?>" id="modelo" name="modelo"
                               placeholder="Modelo">
                    </div>
					 <div class="form-group">
                        <label for="qtd">Quantidade</label>
                        <input type="text" class="form-control" value="<?php echo $data->qtd ?>" id="qtd" name="qtd"
                               placeholder="Quantidade">
                    </div>
					 <div class="form-group">
                        <label for="valorunit">Valor Unitário</label>
                        <input type="text" class="form-control" value="<?php echo $data->valorunit ?>" id="valorunit" name="valorunit"
                               placeholder="Valor Unitário">
                    </div>
                    
                    <input type="hidden" name="id" value="<?php echo $data->id ?>">
                    <button type="submit" class="btn btn-primary">Alterar</button>
                </form>
            </div>
        </div>
    </div>
</div>