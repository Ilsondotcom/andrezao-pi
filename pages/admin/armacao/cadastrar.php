<?php

if (isset($_POST)) {

    $armacao = new Armacao();
    $result = $armacao->add($_POST);

    if ($result) {
        echo '<script>alert("Cadastrado com sucesso!");location.href = "/admin/?pag=armacao&acao=listar";</script>';
    } else {
        echo '<script>alert("Erro ao cadastrar!");location.href = "/admin/?pag=armacao&acao=cadastrar";</script>';
    }
} else {
    include_once 'pages/erros/erro403.php';
}
?>