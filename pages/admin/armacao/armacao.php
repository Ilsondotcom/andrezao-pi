<?php
    if(!isset($_REQUEST['acao'])){
        include_once 'listar.php';
    }else{
        switch ($_REQUEST['acao']){
            case 'listar':
                include_once 'listar.php';
                break;
            case 'adicionar':
                include_once 'adicionar.php';
                break;
            case 'editar':
                include_once 'editar.php';
                break;
            case 'cadastrar':
                include_once 'cadastrar.php';
                break;
            case 'alterar':
                include_once 'alterar.php';
                break;
            case 'excluir':
                include_once 'excluir.php';
                break;
            default:
                include_once 'pages/erros/erro404.php';
                break;
        }
    }
?>