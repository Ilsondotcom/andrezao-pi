<div class="row">
    <div class="col-xs-12">
        <h1>Armação</h1>
        <table class="table table-striped" border="1">
            <thead>
            <tr>
                <th>#</th>
                <th>Marca</th>
				<th>Modelo</th>
                <th>Quantidade</th>
				<th>Valor Unitário</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $armacao = new Armacao();
                $result = $armacao->findAll();

                foreach ($result as $row):
            ?>
            <tr>
                <td><?php echo $row->id; ?></td>
                <td><?php echo $row->marca; ?></td>
				<td><?php echo $row->modelo; ?></td>
                <td><?php echo $row->qtd; ?></td>
				<td><?php echo $row->valorunit ; ?></td>
                <td>
                    <a href="/admin.php?pag=armacao&acao=editar&id=<?php echo $row->id; ?>">Editar</a> |
                    <a onclick="return confirm('Deseja realmente excluir esse registro?')" href="/admin.php?pag=armacao&acao=excluir&id=<?php echo $row->id; ?>">Excluir</a>
                </td>
            </tr>
            <?php
                endforeach;
            ?>
            </tbody>
        </table>
    </div>
</div>
