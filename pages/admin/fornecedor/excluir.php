<?php

if (isset($_GET)) {

    $fornecedor = new Fornecedor();
    $result = $fornecedor->remove($_GET['id']);

    if ($result) {
        echo '<script>alert("Excluído com sucesso!");location.href = "/admin.php?pag=fornecedor&acao=listar";</script>';
    } else {
        echo '<script>alert("Erro ao excluir!");location.href = "/admin.php?pag=fornecedor&acao=listar";</script>';
    }
} else {
    include_once 'pages/erros/erro403.php';
}
?>