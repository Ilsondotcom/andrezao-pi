<?php

if (isset($_POST)) {

    $fornecedor = new Fornecedor();
    $result = $fornecedor->add($_POST);

    if ($result) {
        echo '<script>alert("Cadastrado com sucesso!");location.href = "/admin.php?pag=fornecedor&acao=listar";</script>';
    } else {
        echo '<script>alert("Erro ao cadastrar!");location.href = "/admin.php?pag=fornecedor&acao=cadastrar";</script>';
    }
} else {
    include_once 'pages/erros/erro403.php';
}
?>