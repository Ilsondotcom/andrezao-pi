<?php
    $fornecedor = new Fornecedor();
    $data = $fornecedor->findOneBy($_GET['id']);
?>
<div class="row">
    <div class="col-xs-12">
        <h1>Fornecedor-</h1>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Registro de Fornecedor</h3>
            </div>
            <div class="panel-body">
                <form action="/admin.php?pag=fornecedor&acao=alterar&id=<?php echo $data->id ?>" method="POST">
                    <div class="form-group">
                        <label for="razaosocial">Razão Social</label>
                        <input type="text" class="form-control" value="<?php echo $data->razaosocial ?>" id="razaosocial" name="razaosocial"
                               placeholder="Razão Social">
                    </div>
                    <div class="form-group">
                        <label for="cnpj">CNPJ</label>
                        <input type="text" class="form-control" value="<?php echo $data->cnpj ?>" id="cnpj" name="cnpj"
                               placeholder="CNPJ">
                    </div>
                    </div>
					 <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="text" class="form-control" value="<?php echo $data->email ?>" id="email" name="email"
                               placeholder="E-mail">
                    </div>
                    <div class="form-group">
                        <label for="telfixo">Telefone Fixo</label>
                        <input type="text" class="form-control" value="<?php echo $data->telfixo ?>" id="telfixo"
                               name="telfixo" placeholder="Telefone Fixo">
                    </div>
					<div class="form-group">
                        <label for="cep">CEP</label>
                        <input type="text" class="form-control" value="<?php echo $data->cep ?>" id="cep"
                               placeholder="CEP" name="cep">
                    </div>
					<div class="form-group">
                        <label for="logradouro">Logradouro</label>
                        <input type="text" class="form-control" value="<?php echo $data->logradouro ?>" id="logradouro"
                               placeholder="Logradouro" name="logradouro">
                    </div>
					<div class="form-group">
                        <label for="bairro">Bairro</label>
                        <input type="text" class="form-control" value="<?php echo $data->bairro ?>" id="bairro"
                               placeholder="Bairro" name="bairro">
                    </div>
					<div class="form-group">
                        <label for="cidade">Cidade</label>
                        <input type="text" class="form-control" value="<?php echo $data->cidade ?>" id="cidade"
                               placeholder="Cidade" name="cidade">
                    </div>
						<div class="form-group">
                        <label for="estado">Estado</label>
                        <input type="text" class="form-control" value="<?php echo $data->estado ?>" id="estado"
                               placeholder="Estado" name="estado">
                    </div>
                    <input type="hidden" name="id" value="<?php echo $data->id ?>">
                    <button type="submit" class="btn btn-primary">Alterar</button>
                </form>
            </div>
        </div>
    </div>
</div>