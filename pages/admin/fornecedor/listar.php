<?php
?>
<div class="row">
    <div class="col-xs-12">
        <h1>Fornecedor</h1>
        <table class="table table-striped" border="1">
            <thead>
            <tr>
                <th>#</th>
                <th>Razão Social</th>
				<th>CNPJ</th>
				<th>E-mail</th>
				<th>Tel. Fixo</th>
                <th>CEP</th>
				<th>Logradouro</th>
                <th>Bairro</th>
				<th>Cidade</th>
                <th>Estado</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $fornecedor = new Fornecedor();
                $result = $fornecedor->findAll();

                foreach ($result as $row):
            ?>
            <tr>
                <td><?php echo $row->id; ?></td>
                <td><?php echo $row->razaosocial; ?></td>
				<td><?php echo $row->cnpj; ?></td>
				<td><?php echo $row->email; ?></td>
                <td><?php echo $row->telfixo; ?></td>
                <td><?php echo $row->cep; ?></td>
				<td><?php echo $row->logradouro; ?></td>
                <td><?php echo $row->bairro; ?></td>
				<td><?php echo $row->cidade; ?></td>
                <td><?php echo $row->estado; ?></td>
                <td>
                    <a href="/zooin/?pag=fornecedor&acao=editar&id=<?php echo $row->id; ?>">Editar</a> |
                    <a onclick="return confirm('Deseja realmente excluir esse registro?')" href="/admin.php?pag=fornecedor&acao=excluir&id=<?php echo $row->id; ?>">Excluir</a>
                </td>
            </tr>
            <?php
                endforeach;
            ?>
            </tbody>
        </table>
    </div>
</div>
