<div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Zooin</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li class="<?php if(!isset($_GET['pag']) || $_GET['pag'] == 'principal'){ echo 'active'; }  ?>">
                <a href="?pag=principal">Principal</a>
            </li>
            <!-- Dropdown de Clientes -->
            <li class="dropdown <?php if(isset($_GET['pag']) && $_GET['pag'] == 'clientes'){ echo 'active'; }  ?>">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Clientes <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="?pag=clientes&acao=listar">Listar Clientes</a></li>
                    <li><a href="?pag=clientes&acao=adicionar">Adicionar Novo</a></li>
                </ul>
            </li>
            <!-- Dropdown de Fornecedores -->
            <li class="dropdown <?php if(isset($_GET['pag']) && $_GET['pag'] == 'fornecedor'){ echo 'active'; }  ?>">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Fornecedores <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="?pag=fornecedor&acao=listar">Listar Fornecedores</a></li>
                    <li><a href="?pag=fornecedor&acao=adicionar">Adicionar Novo</a></li>
                </ul>
            </li>
            <!-- Dropdown de Armações -->
            <li class="dropdown <?php if(isset($_GET['pag']) && $_GET['pag'] == 'armacao'){ echo 'active'; }  ?>">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Armações <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="?pag=armacao&acao=listar">Listar Armações</a></li>
                    <li><a href="?pag=armacao&acao=adicionar">Adicionar Novo</a></li>
                </ul>
            </li>
            <!-- Dropdown de Armações -->
            <li class="dropdown <?php if(isset($_GET['pag']) && $_GET['pag'] == 'lente'){ echo 'active'; }  ?>">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Lentes <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="?pag=lente&acao=listar">Listar Armações</a></li>
                    <li><a href="?pag=lente&acao=adicionar">Adicionar Novo</a></li>
                </ul>
            </li>
            <li>
                <a href="/">Voltar para Página</a>
            </li>
        </ul>
    </div><!--/.nav-collapse -->
</div>