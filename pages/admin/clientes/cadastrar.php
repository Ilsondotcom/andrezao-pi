<?php

if (isset($_POST)) {

    $clientes = new Clientes();
    $result = $clientes->add($_POST);

    if ($result) {
        if(!isset($_REQUEST['from'])){
            echo '<script>alert("Cadastrado com sucesso!");location.href = "admin.php?pag=clientes&acao=listar";</script>';
        } else {
            session_start();
            $_SESSION['user'] = $clientes->login($_POST['email'], $_POST['senha']);
            echo '<script>alert("Cadastrado com sucesso!");location.href = "/";</script>';
        }
    } else {
        echo '<script>alert("Erro ao cadastrar!");location.href = "admin.php?pag=clientes&acao=cadastrar";</script>';
    }
} else {
    include_once 'pages/admin/erros/erro403.php';
}
?>
