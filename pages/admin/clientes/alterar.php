<?php

if (isset($_POST)) {

    $clientes = new Clientes();
    $result = $clientes->update($_POST);

    if ($result) {
        echo '<script>alert("Alterado com sucesso!");location.href = "/admin.php?pag=clientes&acao=listar";</script>';
    } else {
        echo '<script>alert("Erro ao alterar!");location.href = "/admin.php?pag=clientes&acao=editar&id='. $_POST['codigo'].'";</script>';
    }
} else {
    include_once 'pages/erros/erro403.php';
}
?>
