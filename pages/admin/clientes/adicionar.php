<script type="text/javascript">
function validaCampo(){
	
if(document.cadastro.nome.value=="")
	{
	alert("O Campo nome é obrigatório!");
	return false;
	}
else if(document.cadastro.CPF.value=="")
	{
	alert("O Campo CPF é obrigatório!");
	return false;
	}
else  
if(document.cadastro.email.value=="")
	{
	alert("O Campo email é obrigatório!");
	return false;
	}  
	if(document.cadastro.cel.value=="")
	{
	alert("O Campo Celular é obrigatório!");
	return false;
	}
	if(document.cadastro.telfixo.value=="")
	{
	alert("O Campo Telefone Fixo é obrigatório!");
	return false;
	}
else
	if(document.cadastro.Logradouro.value=="")
	{
	alert("O Campo Logradouro é obrigatório!");
	return false;
	}
	if(document.cadastro.CEP.value=="")
	{
	alert("O Campo CEP é obrigatório!");
	return false;
	}
else
	if(document.cadastro.cidade.value=="")
	{
	alert("O Campo Cidade é obrigatório!");
	return false; 
	}
else
	if(document.cadastro.estado.value=="")
	{
	alert("O Campo Estado é obrigatório!");
	return false;
	}
else
	if(document.cadastro.bairro.value=="")
	{
	alert("O Campo Bairro é obrigatório!");
	return false;
	}
else
return true;
}
</script>

<script language="JavaScript">
 function mascara(t, mask){
 var i = t.value.length;
 var saida = mask.substring(1,0);
 var texto = mask.substring(i)
 if (texto.substring(0,1) != saida){
 t.value += texto.substring(0,1);
 }}</script>   
   <div class="row">
    <div class="col-xs-12">
        <h1>Clientes</h1>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Registro de Clientes</h3>
            </div>
            <div class="panel-body">
                <form action="/admin.php?pag=clientes&acao=cadastrar" method="POST" onsubmit="return validaCampo(); return false;">
                    <div class="form-group">
                        <label for="nome">Nome:</label>
                        <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome">
                    </div>
                    <div class="form-group">
                        <label for="cpf">CPF</label>
                        <input type="text" class="form-control" id="cpf" name="cpf" onkeypress="mascara(this, '###.###.###-##')" maxlength="14" placeholder="CPF">
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="text" class="form-control" id="E-mail" name="email" placeholder="E-mail">
                    </div>
                    <div class="form-group">
                        <label for="email">Senha</label>
                        <input type="text" class="form-control" id="senha" name="senha" placeholder="Senha">
                    </div>
                    <div class="form-group">
                        <label for="sexo">Sexo</label>
                        <input type="radio" class="form-control" id="Sexo" name="sexo" value="Masculino" checked="checked" />Masculino
                        <input type="radio" class="form-control" id="Sexo" name="sexo" Value="Feminino" />Feminino
                    </div> 
					<div class="form-group">
                        <label for="cel">Celular</label>
                        <input type="text" class="form-control" id="cel" name="cel" onkeypress="mascara(this, '## #####-####')" maxlength="13" placeholder="Tel. Celular">
                    </div>
                    <div class="form-group">
                        <label for="telfixo">Telefone Fixo</label>
                        <input type="text" class="form-control" id="telfixo" name="telfixo" onkeypress="mascara(this, '## ####-####')" maxlength="12" placeholder="Tel. Fixo">
                    </div>
                    <div class="form-group">
                        <label for="cep">CEP</label>
                        <input type="text" class="form-control" id="cep" name="cep" onkeypress="mascara(this, '#####-###')" maxlength="9" placeholder="CEP">
                    </div> 
                    <div class="form-group">
                        <label for="logradouro">Logradouro</label>
                        <input type="text" class="form-control" id="logradouro" name="logradouro" placeholder="Logradouro">
                    </div>
                    <div class="form-group">
                        <label for="bairro">Bairro</label>
                        <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Bairro">
                    </div> 
                    <div class="form-group">
                        <label for="cidade">Cidade</label>
                        <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Cidade">
                    </div> 
                    <div class="form-group">
                        <label for="Estado">Estado</label>
                        <select name="estado" id="estado">
							<option>Selecione...</option>
							<option value="AC">AC</option>
							<option value="AL">AL</option>
							<option value="AP">AP</option>
							<option value="AM">AM</option>
							<option value="BA">BA</option>
							<option value="CE">CE</option>
							<option value="ES">ES</option>
							<option value="DF">DF</option>
							<option value="MA">MA</option>
							<option value="MT">MT</option>
							<option value="MS">MS</option>
							<option value="MG">MG</option>
							<option value="PA">PA</option>
							<option value="PB">PB</option>
							<option value="PR">PR</option>
							<option value="PE">PE</option>
							<option value="PI">PI</option>
							<option value="RJ">RJ</option>
							<option value="RN">RN</option>
							<option value="RS">RS</option>
							<option value="RO">RO</option>
							<option value="RR">RR</option>
							<option value="SC">SC</option>
							<option value="SP">SP</option>
							<option value="SE">SE</option>
							<option value="TO">TO</option>
          				</select>
                    </div> 
                    <div class="form-group">
                        <input type="checkbox" class="form-control" id="news" name="news" value="Sim" checked="checked" /> Desejo receber novidades e informações sobre o conteúdo deste site.
                    </div>
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </form>
            </div>
        </div>
    </div>
</div>