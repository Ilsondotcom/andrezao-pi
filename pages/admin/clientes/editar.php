<?php
    $clientes = new Clientes();
    $data = $clientes->findOneBy($_GET['id']);
?>
<div class="row">
    <div class="col-xs-12">
        <h1>Clientes</h1>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Registro de Clientes</h3>
            </div>
            <div class="panel-body">
                <form action="/admin.php?pag=clientes&acao=alterar&id=<?php echo $data->id ?>" method="POST">
                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input type="text" class="form-control" value="<?php echo $data->nome ?>" id="nome" name="nome"
                               placeholder="Nome">
                    </div>
                    <div class="form-group">
                        <label for="cpf">CPF</label>
                        <input type="text" class="form-control" value="<?php echo $data->cpf ?>" id="cpf" name="cpf"
                               placeholder="CPF">
                    </div>
					<div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="text" class="form-control" value="<?php echo $data->email ?>" id="email" name="email"
                               placeholder="email">
                    </div>
                    <div class="form-group">
                        <label for="sexo">Sexo</label>
                        <input type="text" class="form-control" value="<?php echo $data->sexo ?>" id="sexo" name="sexo" placeholder="sexo">
                    </div>
                    <div class="form-group">
                        <label for="cel">Tel. Celular</label>
                        <input type="text" class="form-control" value="<?php echo $data->cel ?>" id="cel"
                               name="cel" placeholder="Tel.Celular">
                    </div>
					 <div class="form-group">
                        <label for="telfixo">Tel. Fixo</label>
                        <input type="text" class="form-control" value="<?php echo $data->telfixo ?>" id="telfixo"
                               name="telfixo" placeholder="Tel. Fixo">
                    </div>
                    <div class="form-group">
                        <label for="cep">CEP</label>
                        <input type="text" class="form-control" value="<?php echo $data->cep ?>" id="cep"
                               placeholder="cep" name="cep">
                    </div>
					<div class="form-group">
                        <label for="logradouro">Logradouro</label>
                        <input type="text" class="form-control" value="<?php echo $data->logradouro ?>" id="logradouro"
                               placeholder="logradouro" name="logradouro">
                    </div>
					<div class="form-group">
                        <label for="bairro">Bairro</label>
                        <input type="text" class="form-control" value="<?php echo $data->bairro ?>" id="bairro"
                               placeholder="bairro" name="bairro">
                    </div>
					<div class="form-group">
                        <label for="cidade">Cidade</label>
                        <input type="text" class="form-control" value="<?php echo $data->cidade ?>" id="cidade"
                               placeholder="cidade" name="cidade">
                    </div>
						<div class="form-group">
                        <label for="estado">Estado</label>
                        <input type="text" class="form-control" value="<?php echo $data->estado ?>" id="estado"
                               placeholder="estado" name="estado">
                    </div>
						<div class="form-group">
                        <label for="news">News</label>
                        <input type="text" class="form-control" value=" <?php echo $data->news ?>" id="news"
                               placeholder="news" name="news">
                    </div>
                    <input type="hidden" name="id" value="<?php echo $data->id ?>">
                    <button type="submit" class="btn btn-primary">Alterar</button>
                </form>
            </div>
        </div>
    </div>
</div>