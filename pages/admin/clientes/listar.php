<?php

?>
<div class="row">
    <div class="col-xs-12">
        <h1>Clientes</h1>
        <table class="table table-striped" border="1">
            <thead>
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>CPF</th>
				<th>E-mail</th>
				<th>Sexo</th>
				<th>Tel. Celular</th>
                <th>Tel. Fixo</th>
                <th>Cep</th>
				<th>Logradouro</th>
                <th>Bairro</th>
                <th>Cidade</th>
                <th>Estado</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $clientes = new Clientes();
                $result = $clientes->findAll();

                foreach ($result as $row):
            ?>
            <tr>
                <td><?php echo $row->id; ?></td>
                <td><?php echo $row->nome; ?></td>
                <td><?php echo $row->cpf; ?></td>
				<td><?php echo $row->email; ?></td>
				<td><?php echo $row->sexo; ?></td>
                <td><?php echo $row->cel; ?></td>
                <td><?php echo $row->telfixo; ?></td>
                <td><?php echo $row->cep; ?></td>
				<td><?php echo $row->logradouro; ?></td>
                <td><?php echo $row->bairro; ?></td>
				<td><?php echo $row->cidade; ?></td>
                <td><?php echo $row->estado; ?></td>
                <td>
                    <a href="/admin.php?pag=clientes&acao=editar&id=<?php echo $row->id; ?>">Editar</a> |
                    <a onclick="return confirm('Deseja realmente excluir esse registro?')" href="/admin.php?pag=clientes&acao=excluir&id=<?php echo $row->id; ?>">Excluir</a>
                </td>
            </tr>
            <?php
                endforeach;
            ?>
            </tbody>
        </table>
    </div>
</div>
