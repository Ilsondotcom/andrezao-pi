<?php

/**
 * Class Armação
 * @package Zooin\Src\Resource
 * Classe que vai fazer as interações no banco de dados das armações
 */
class Armacao
{
    public function findAll()
    {
        $query = "SELECT * FROM armacao";
        $stmt = DB::prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function findOneBy($id)
    {
        $query = "SELECT * FROM armacao WHERE id = $id";
        $stmt = DB::prepare($query);
        $stmt->execute();
        $result = $stmt->fetch();

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function add($data)
    {
        $query = "INSERT INTO armacao (marca, modelo, qtd, valorunit, descricao) VALUES (?,?,?,?,?)";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $data['marca'],
            $data['modelo'],
            $data['qtd'],
			$data['valorunit'],
			$data['descricao'],
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function update($data)
    {
        $query = "UPDATE armacao set marca = ?, modelo = ?, qtd = ?, valorunit = ? WHERE id = ?";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $data['marca'],
            $data['modelo'],
            $data['qtd'],
			$data['valorunit'],
            $data['id']
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function remove($id)
    {
        $query = "DELETE FROM armacao WHERE id = ?";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $id
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}