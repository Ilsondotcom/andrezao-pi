<?php


/**
 * Class Carrinho
 * @package Zooin\Src\Resource
 * Classe que vai fazer as interações no banco de dados do carrinho de compras
 */
class Carrinho
{
    public function count($id)
    {
        $query = "SELECT * FROM carrinho WHERE cliente = $id";
        $stmt = DB::prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return count($result);
    }

    public function findAll($id)
    {
        $query = "SELECT * FROM carrinho WHERE cliente = $id";
        $stmt = DB::prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function findOneBy($id)
    {
        $query = "SELECT * FROM carrinho WHERE id = $id";
        $stmt = DB::prepare($query);
        $stmt->execute();
        $result = $stmt->fetch();

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function add($data)
    {
        $query = "INSERT INTO carrinho (produto, preco, quantidade, cliente) VALUES (?,?,?,?)";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $data['produto'],
            $data['preco'],
            $data['quantidade'],
            $data['cliente'],
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function remove($id)
    {
        $query = "DELETE FROM carrinho WHERE id = ?";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $id
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}