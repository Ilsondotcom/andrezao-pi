<?php

/**
 * Class Fornecedores
 * @package Zooin\Src\Resource
 * Classe que vai fazer as interações no banco de dados dos fornecedores
 */
class Fornecedor
{
    public function findAll()
    {
        $query = "SELECT * FROM fornecedor";
        $stmt = DB::prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function findOneBy($id)
    {
        $query = "SELECT * FROM fornecedor WHERE id = $id";
        $stmt = DB::prepare($query);
        $stmt->execute();
        $result = $stmt->fetch();

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function add($data)
    {
        $query = "INSERT INTO fornecedor (razaosocial, cnpj, email, telfixo, cep, logradouro, bairro, cidade, estado) VALUES (?,?,?,?,?,?,?,?,?)";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $data['razaosocial'],
            $data['cnpj'],
            $data['email'],
			$data['telfixo'],
			$data['cep'],
			$data['logradouro'],
			$data['bairro'],
			$data['cidade'],
			$data['estado'],
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function update($data)
    {
        $query = "UPDATE fornecedor set razaosocial = ?, cnpj = ?, email = ?, telfixo = ?, cep = ?, logradouro = ?, bairro = ?, cidade = ?, estado = ? WHERE id = ?";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $data['razaosocial'],
            $data['cnpj'],
            $data['email'],
			$data['telfixo'],
			$data['cep'],
			$data['logradouro'],
			$data['bairro'],
			$data['cidade'],
			$data['estado'],
            $data['id']
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function remove($id)
    {
        $query = "DELETE FROM fornecedor WHERE id = ?";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $id
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}