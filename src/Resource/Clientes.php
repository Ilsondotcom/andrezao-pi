<?php

/**
 * Class Clientes
 * @package Zooin\Src\Resource
 * Classe que vai fazer as interações no banco de dados dos clientes
 */
class Clientes
{
    public function findAll()
    {
        $query = "SELECT * FROM cliente";
        $stmt = DB::prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function findOneBy($id)
    {
        $query = "SELECT * FROM cliente WHERE id = $id";
        $stmt = DB::prepare($query);
        $stmt->execute();
        $result = $stmt->fetch();

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function login($user, $password)
    {
        $query = "SELECT * FROM cliente WHERE email = '$user' AND senha = '$password'";
        $stmt = DB::prepare($query);
        $stmt->execute();
        $result = $stmt->fetch();

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function add($data)
    {
        $query = "INSERT INTO cliente (nome, cpf, email, senha, sexo, cel, telfixo, cep, logradouro, bairro, cidade, estado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $data['nome'],
            $data['cpf'],
            $data['email'],
            $data['senha'],
			$data['sexo'],
			$data['cel'],
			$data['telfixo'],
			$data['cep'],
			$data['logradouro'],
			$data['bairro'],
			$data['cidade'],
			$data['estado'],
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function update($data)
    {
        $query = "UPDATE cliente set nome = ?, cpf = ?, email = ?, sexo = ?, cel = ?, telfixo = ?, cep = ?, logradouro = ?, bairro = ?, cidade = ?, estado = ? WHERE id = ?";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $data['nome'],
            $data['cpf'],
            $data['email'],
			$data['sexo'],
			$data['cel'],
			$data['telfixo'],
			$data['cep'],
			$data['logradouro'],
			$data['bairro'],
			$data['cidade'],
			$data['estado'],
            $data['id']
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function remove($id)
    {
        $query = "DELETE FROM cliente WHERE id = ?";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $id
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}