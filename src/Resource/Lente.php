<?php


/**
 * Class Clientes
 * @package Zooin\Src\Resource
 * Classe que vai fazer as interações no banco de dados dos clientes
 */
class Lente
{
    public function findAll()
    {
        $query = "SELECT * FROM lente";
        $stmt = DB::prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function findOneBy($id)
    {
        $query = "SELECT * FROM lente WHERE id = $id";
        $stmt = DB::prepare($query);
        $stmt->execute();
        $result = $stmt->fetch();

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function add($data)
    {
        $query = "INSERT INTO lente (marca, modelo, grau, qtd, valorunit) VALUES (?,?,?,?,?)";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $data['marca'],
            $data['modelo'],
            $data['grau'],
			$data['qtd'],
			$data['valorunit'],
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function update($data)
    {
        $query = "UPDATE lente set marca = ?, modelo = ?, grau = ?, qtd = ?, valorunit = ? WHERE id = ?";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $data['marca'],
            $data['modelo'],
            $data['grau'],
			$data['qtd'],
			$data['valorunit'],
            $data['id']
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function remove($id)
    {
        $query = "DELETE FROM lente WHERE id = ?";
        $stmt = DB::prepare($query);
        $result = $stmt->execute(array(
            $id
        ));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}