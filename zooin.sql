-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 08-Dez-2017 às 21:39
-- Versão do servidor: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zooin`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `armacao`
--

DROP TABLE IF EXISTS `armacao`;
CREATE TABLE IF NOT EXISTS `armacao` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `marca` varchar(20) NOT NULL,
  `modelo` varchar(20) NOT NULL,
  `qtd` int(5) NOT NULL,
  `valorunit` int(7) NOT NULL,
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `armacao`
--

INSERT INTO `armacao` (`id`, `marca`, `modelo`, `qtd`, `valorunit`, `descricao`) VALUES
(2, 'LeoLeo', 'LeoLeo1', 100, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in libero euismod, scelerisque lacus quis, scelerisque mauris. In dapibus, risus vel rhoncus dignissim, dolor dui iaculis felis, eu luctus dolor nibh non eros.'),
(3, 'Ray Ban', 'Aviador', 50, 255, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in libero euismod, scelerisque lacus quis, scelerisque mauris. In dapibus, risus vel rhoncus dignissim, dolor dui iaculis felis, eu luctus dolor nibh non eros.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `carrinho`
--

DROP TABLE IF EXISTS `carrinho`;
CREATE TABLE IF NOT EXISTS `carrinho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto` varchar(200) NOT NULL,
  `preco` varchar(10) NOT NULL,
  `quantidade` int(3) NOT NULL,
  `cliente` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `carrinho`
--

INSERT INTO `carrinho` (`id`, `produto`, `preco`, `quantidade`, `cliente`) VALUES
(3, 'Aviador', '255', 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `cpf` char(14) NOT NULL,
  `email` varchar(20) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `sexo` varchar(9) NOT NULL,
  `cel` varchar(13) NOT NULL,
  `telfixo` varchar(12) NOT NULL,
  `cep` char(9) NOT NULL,
  `logradouro` varchar(45) NOT NULL,
  `bairro` varchar(45) NOT NULL,
  `cidade` varchar(32) NOT NULL,
  `estado` char(2) NOT NULL,
  `admin` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `cpf`, `email`, `senha`, `sexo`, `cel`, `telfixo`, `cep`, `logradouro`, `bairro`, `cidade`, `estado`, `admin`) VALUES
(2, 'Andre Felipe', '619.298.403-49', 'email@gmail.com', '123456', 'Masculino', '61 99557-1332', '61 3212-2357', '71939-360', 'Quadra 203', 'Sul (Águas Claras)', 'Brasília', 'DF', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedor`
--

DROP TABLE IF EXISTS `fornecedor`;
CREATE TABLE IF NOT EXISTS `fornecedor` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `razaosocial` varchar(45) NOT NULL,
  `cnpj` varchar(18) NOT NULL,
  `email` varchar(20) NOT NULL,
  `telfixo` varchar(12) NOT NULL,
  `cep` varchar(9) NOT NULL,
  `logradouro` varchar(45) NOT NULL,
  `bairro` varchar(45) NOT NULL,
  `cidade` varchar(32) NOT NULL,
  `estado` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

DROP TABLE IF EXISTS `funcionario`;
CREATE TABLE IF NOT EXISTS `funcionario` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `cpf` bigint(11) NOT NULL,
  `email` varchar(20) NOT NULL,
  `sexo` enum('M','F') NOT NULL,
  `cel` int(9) NOT NULL,
  `telfixo` int(9) NOT NULL,
  `cep` int(8) NOT NULL,
  `logradouro` varchar(45) NOT NULL,
  `bairro` varchar(45) NOT NULL,
  `cidade` varchar(32) NOT NULL,
  `estado` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `lente`
--

DROP TABLE IF EXISTS `lente`;
CREATE TABLE IF NOT EXISTS `lente` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `marca` varchar(20) NOT NULL,
  `modelo` varchar(20) NOT NULL,
  `grau` int(3) NOT NULL,
  `qtd` int(5) NOT NULL,
  `valorunit` int(7) NOT NULL,
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `lente`
--

INSERT INTO `lente` (`id`, `marca`, `modelo`, `grau`, `qtd`, `valorunit`, `descricao`) VALUES
(1, 'LeoLeo', 'LeoLeo1', 12, 200, 7, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in libero euismod, scelerisque lacus quis, scelerisque mauris. In dapibus, risus vel rhoncus dignissim, dolor dui iaculis felis, eu luctus dolor nibh non eros.');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
