<?php
include_once 'src/Helper/index.php';
include_once 'src/Resource/includeResource.php';
session_start();
if (isset($_SESSION['user']) && $_SESSION['user']->admin === 0) {
    echo '<script> window.location = "/"; </script>';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Sistema de Gerenciamento de Ótica</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="/assets/css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/assets/css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <?php
    //Modularizar o menu, edita em um único arquivo e altera em todos
    include_once 'pages/admin/menu/menu.php';
    ?>
</nav>
<div class="container theme-showcase" role="main">
    <?php
    /*
     * Padroniza as urls como /?pag=pagina-de-destino para não precisar ficar recarregando tudo o tempo todo,
     * facilitar a codificação
     */
    if (isset($_GET['pag'])) {
        switch ($_GET['pag']) {
            case 'principal':
                include_once 'pages/admin/principal/principal.php';
                break;
            case 'clientes':
                include_once 'pages/admin/clientes/clientes.php';
                break;
            case 'fornecedor':
                include_once 'pages/admin/fornecedor/fornecedor.php';
                break;
            case 'armacao':
                include_once 'pages/admin/armacao/armacao.php';
                break;
            case 'lente':
                include_once 'pages/admin/lente/lente.php';
                break;
            case 'produto':
                include_once 'pages/client/conteudo/adicionarCarrinho.php';
                break;
            default:
                include_once 'pages/admin/erros/erro404.php';
                break;
        }
    } else {
        include_once 'pages/admin/principal/principal.php';
    }
    ?>
</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/docs.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
